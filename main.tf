resource "aws_instance" "example" {
 ami           = "ami-0005e0cfe09cc9050"
 instance_type = "t2.micro"
 subnet_id     = data.aws_vpcs.new_vpc.id


 tags = {
    Name = "test-ec2-instance"
 }
}
resource "aws_s3_bucket" "example_bucket" {
 bucket = "test-bucket"
 
 tags = {
    Name = "test-bucket"
 }
}